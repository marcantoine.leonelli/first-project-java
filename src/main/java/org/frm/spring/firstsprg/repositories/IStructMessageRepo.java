package org.frm.spring.firstsprg.repositories;

import org.frm.spring.firstsprg.model.StructMessage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IStructMessageRepo extends JpaRepository<StructMessage, Long> {
    public List<StructMessage> findByAuteur(String auteur);
}
