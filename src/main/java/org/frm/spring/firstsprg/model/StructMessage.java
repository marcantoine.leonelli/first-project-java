package org.frm.spring.firstsprg.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.*;
import org.frm.spring.firstsprg.repositories.IStructMessageRepo;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Date;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "message")
@Entity
public class StructMessage {

    @Id
    @GeneratedValue
    private int id;

    private String auteur;
    private Date dateCreation;
    private String content;

}
