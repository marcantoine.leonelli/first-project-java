package org.frm.spring.firstsprg;

import jakarta.ws.rs.QueryParam;
import org.frm.spring.firstsprg.model.StructMessage;
import org.frm.spring.firstsprg.repositories.IStructMessageRepo;
import org.frm.spring.firstsprg.services.IServiceUn;
import org.frm.spring.firstsprg.services.ServiceUnImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
public class EchoController {

    private IServiceUn service;
    private IStructMessageRepo repo;

    public EchoController(IServiceUn service, IStructMessageRepo repo) {
        this.service = service;
        this.repo = repo;
        System.out.println("EchoController created");
    }


    @GetMapping("/echo")
    public String echo(@QueryParam("message") String message) {
        Date dt = new Date();

        return String.format("Echo: %1$s, %2$s", message, dt.toString());
    }

    @GetMapping("/echo/{message}")
    public String echoPath(@PathVariable String message) {
        Date dt = new Date();

        return String.format("Echo: %1$s, %2$s", message, dt.toString());
    }

    @PostMapping("/echo")
    public ResponseEntity<String> echoPost(@RequestBody String message) {
        Date dt = new Date();

        ResponseEntity<String> responseEntity =
                new ResponseEntity<>(String.format("Echo: %1$s, %2$s", message, dt.toString()), HttpStatus.OK);
        return responseEntity;
    }

    @GetMapping("/newecho")
    public ResponseEntity<StructMessage> echoSE() {
        StructMessage str = new StructMessage();

        str.setAuteur("toto");
        str.setContent("encore un message ");
        str.setDateCreation(new Date());

        repo.save(str);
        //repo.findById()

        ResponseEntity<StructMessage> responseEntity =
                new ResponseEntity<>(str, HttpStatus.OK);
        return responseEntity;
    }
}
