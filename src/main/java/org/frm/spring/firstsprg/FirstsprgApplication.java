package org.frm.spring.firstsprg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FirstsprgApplication {

	public static void main(String[] args) {
		SpringApplication.run(FirstsprgApplication.class, args);
	}

}
