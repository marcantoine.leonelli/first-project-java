package org.frm.spring.firstsprg.tools;

import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class Calculette {
    private static final Logger log = LoggerFactory.getLogger(Calculette.class);

    @Getter
    private int total = 0;

    public Calculette() {
        log.info("Calculette created");
    }

    public void moins (int n) {
        log.info("moins({})", n);
        total -= n;
    }

    public void plus (int n) {
        total += n;
    }
}
